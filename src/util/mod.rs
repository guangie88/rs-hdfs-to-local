use failure::Error;
use std;
use std::io::Read;
use std::process::{Child, ChildStdout, Output};
use std::string;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Fail, Debug)]
#[fail(display = "Dir flag is unexpectedly empty")]
pub struct DirFlagEmpty;

#[derive(Fail, Debug)]
pub enum CmdError {
    #[fail(display = "No output from {}: {}", desc, err_msg)]
    StdoutEmpty { desc: &'static str, err_msg: String },

    #[fail(display = "Stderr is empty")]
    StderrEmpty,

    #[fail(display = "Error reading from pipe: {}", _0)]
    IoError(std::io::Error),

    #[fail(display = "Cannot convert from UTF8 into String: {}", _0)]
    FromUtf8Error(string::FromUtf8Error),

    #[fail(display = "{}: exit code: {:?}, error: {}", desc, code, err_msg)]
    OutputError {
        desc: &'static str,
        code: Option<i32>,
        err_msg: String,
    },
}

pub fn extract_child_stdout(
    child: Child,
    desc: &'static str,
) -> std::result::Result<ChildStdout, CmdError> {
    let (stdout, stderr) = (child.stdout, child.stderr);

    stdout.ok_or_else(|| {
        let err_msg = stderr.ok_or_else(|| CmdError::StderrEmpty).and_then(
            |mut bytes| {
                let mut s = String::new();
                bytes.read_to_string(&mut s).map_err(CmdError::IoError)?;
                Ok(s)
            },
        );

        match err_msg {
            Ok(err_msg) => CmdError::StdoutEmpty {
                desc: desc,
                err_msg: err_msg,
            },
            Err(e) => e,
        }
    })
}

pub fn extract_output_stdout_str(
    output: Output,
    desc: &'static str,
) -> std::result::Result<String, CmdError> {
    if output.status.success() {
        String::from_utf8(output.stdout).map_err(CmdError::FromUtf8Error)
    } else {
        Err(CmdError::OutputError {
            desc: desc,
            code: output.status.code(),
            err_msg: String::from_utf8(output.stderr)
                .map_err(CmdError::FromUtf8Error)?,
        })
    }
}
