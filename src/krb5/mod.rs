extern crate which;

use std::borrow::Cow;
use std::path::PathBuf;
use std::process::{Command, Stdio};

use super::util::{extract_child_stdout, extract_output_stdout_str, Result};

const ECHO: &str = "echo";
const KINIT: &str = "kinit";

#[derive(Debug)]
pub struct Krb5 {
    kinit: PathBuf,
}

#[derive(Deserialize, Debug)]
#[serde(tag = "type", content = "value")]
pub enum Auth<'a> {
    Password(Cow<'a, str>),
    Keytab(Cow<'a, str>),
}

impl Krb5 {
    pub fn new() -> Result<Krb5> {
        let kinit = which::which(KINIT)?;
        Ok(Krb5::with_path(kinit))
    }

    pub fn with_path(kinit: PathBuf) -> Krb5 {
        Krb5 { kinit }
    }

    pub fn kinit(&self, name: &str, auth: &Auth) -> Result<String> {
        let kinit = match *auth {
            Auth::Password(ref pw) => {
                let echo = Command::new(ECHO)
                    .arg(pw.as_ref())
                    .stdout(Stdio::piped())
                    .spawn()?;

                let pw = extract_child_stdout(echo, "echo")?;
                Command::new(&self.kinit).arg(name).stdin(pw).output()
            }

            Auth::Keytab(ref kt) => Command::new(&self.kinit)
                .args(&["-k", "-t", kt.as_ref(), name])
                .output(),
        }?;

        Ok(extract_output_stdout_str(kinit, "kinit")?)
    }
}
